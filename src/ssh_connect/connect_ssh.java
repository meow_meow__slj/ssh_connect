package ssh_connect;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.sql.*;
public class connect_ssh{
 
	/*
	 * lPort:映射到本地的端口
	 * sship：ssh服务ip
	 * sshport：ssh端口，默认22
	 * sshname：ssh用户名
	 * sshpwd：ssh密码
	 * mysqlport：mysql服务端口
	 */
	public static void start_ssh(int lPort,String sship,int sshport,String sshname,
			String sshpwd,int mysqlport){
		JSch jsch=null; 
		Session session = null;  
		try{		
				jsch = new JSch(); 
				//设置ssh的用户名、ip地址、端口，一般默认端口22
				session = jsch.getSession(sshname, sship, sshport);
				session.setPassword(sshpwd);  
				session.setConfig("StrictHostKeyChecking", "no");  
				session.connect();  
				//本地转发到远程
				session.setPortForwardingL(lPort, sship, mysqlport);
				//远程转发到本地
				session.setPortForwardingR(mysqlport,sship, lPort);
				//调试时可以解注
//				System.out.println("ssh连接成功，ssh版本号："+session.getServerVersion());
			}
		catch (Exception e){
				e.printStackTrace();
		}
}

//调试时可以解注
// public static void main(String[] args) throws Exception{
//	 start_ssh(3334,"172.16.81.29",22,"htc","123123",3307);
// }
//
	}
